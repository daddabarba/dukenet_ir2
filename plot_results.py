import argparse
import json

import numpy as np
import matplotlib.pyplot as plt

def read_json(path):
    data = None

    with open(path) as f:    
        data = json.load(f)

    assert(data is not None)
    return data

def extract_values(json_file):
    mrr = []
    ndcg = []

    for k, x in json_file.items():
        mrr.append((k, x["RR"]))
        ndcg.append((k, x["bleu1"]/100))

    assert(len(mrr) == len(ndcg))

    mrr = sorted(mrr, key=lambda x: x[0])
    ndcg = sorted(ndcg, key=lambda x: x[0])

    order, mrr = zip(*mrr)
    order, ndcg = zip(*ndcg)

    return np.array(mrr), np.array(ndcg), order

def get_lengths(query_file):

    data = None
    with open(query_file) as f:
        data = f.readlines()

    assert(data is not None)

    out = {}
    sents = {}
    for x in data:
        x = x.split('\t', 1)

        query_id = x[0].strip()
        length = len(x[1].strip().split(' '))

        assert(query_id not in out)
        out[query_id] = length
        sents[query_id] = x[1].strip()

    return out, sents

def main(args):

    lengths_dict, sents = get_lengths(args.query_file)

    json1 = read_json(args.file1)
    json2 = read_json(args.file2)

    mrr1, ndcg1, order1 = extract_values(json1)
    mrr2, ndcg2, order2 = extract_values(json2)

    assert(order1 == order2)
    assert(mrr1.shape == mrr2.shape)

    order_mrr, mrr_diff = zip(*sorted(zip(order1, mrr1 - mrr2), key=lambda x : x[1], reverse=True))
    order_ndcg, ndcg_diff = zip(*sorted(zip(order1, ndcg1 - ndcg2), key=lambda x : x[1], reverse=True))

    query_min = order_mrr[-1]
    query_max = order_mrr[0]
    print(query_min, query_max, "\n")

    print(f"{args.name1} min, {query_min}: {json1[query_min]}\n")
    print(f"{args.name1} max {query_max}: {json1[query_max]}\n")
    print(f"{args.name2} min {query_min}: {json2[query_min]}\n")
    print(f"{args.name2} max {query_max}: {json2[query_max]}\n")

    print(f"For MRR {args.name1} is better in {order_mrr[:20]}")
    for sent_id in order_mrr[:20]:
        print("\t", sents[sent_id])
    print(f"For MRR {args.name2} is better in {order_mrr[-20:]}")
    for sent_id in order_mrr[::-1][:20]:
        print("\t", sents[sent_id])
    print(f"For bleu1 {args.name1} is better in {order_ndcg[:20]}")
    for sent_id in order_ndcg[:20]:
        print("\t", sents[sent_id])
    print(f"For bleu1 {args.name2} is better in {order_ndcg[-20:]}")
    for sent_id in order_ndcg[::-1][:20]:
        print("\t", sents[sent_id])

    ymin = round(min(np.min(mrr_diff), np.min(ndcg_diff)))
    ymax = round(max(np.max(mrr_diff), np.max(ndcg_diff)))

    fig, ax = plt.subplots(1, 2, constrained_layout=True)
    x_axis = [x for x in range(mrr1.shape[0])]

    # plot
    ax[0].bar(x_axis, mrr_diff, width=1.0, facecolor='navy')
    ax[1].bar(x_axis, ndcg_diff, width=1.0, facecolor='darkgreen')

    # remove ticks
    ax[0].set_xticks([])
    ax[1].set_xticks([])

    # Title
    ax[0].title.set_text(list(json1[list(json1.keys())[0]].keys())[0] + f" {args.name1} - {args.name2}")
    ax[1].title.set_text(list(json1[list(json1.keys())[0]].keys())[2] + f" {args.name1} - {args.name2}")

    # Axis labels
    ax[0].set_xlabel("query")
    ax[0].set_ylabel(f"{list(json1[list(json1.keys())[0]].keys())[0]} delta")

    ax[1].set_xlabel("query")
    ax[1].set_ylabel(f"{list(json1[list(json1.keys())[0]].keys())[2]} delta")

    # rescale
    ax[0].set_ylim([ymin, ymax])
    ax[1].set_ylim([ymin, ymax])

    plt.figure()

    plt.hist(mrr1, bins=100, alpha=0.5, label=args.name1)
    plt.hist(mrr2, bins=100, alpha=0.5, label=args.name2)

    plt.title('MRR Score distribution')
    plt.xlabel('MRR score')
    plt.ylabel('frequency')

    plt.legend()

    zero_start = np.where(np.array(mrr_diff)==0)[0][0]
    zero_end = np.where(np.array(mrr_diff)==0)[0][-1]

    turns = list(map(lambda x : int(x.split('_')[-1]), order_mrr))

    plt.figure()

    plt.hist(turns[0:zero_start-1], bins=100, alpha=0.5, label=f'{args.name1} better')
    plt.hist(turns[zero_end+1:], bins=100, alpha=0.5, label=f'{args.name2} better')

    plt.title('Frquency of turns of queries \nin which algorithm improves over the other')
    plt.xlabel('turn')
    plt.ylabel('frequency')

    plt.legend()

    # Bin lengths

    mrr_binned = get_binned(lengths_dict, mrr_diff, order_mrr, args.n_bins)
    ndcg_binned = get_binned(lengths_dict, ndcg_diff, order_ndcg, args.n_bins)

    plot_binned(mrr_binned, ndcg_binned, "MRR average delta", "BLEU1 average delta")

    plt.show()

def get_binned(lengths_dict, deltas, order, n_bins):
    deltas, bin_values = zip(*[(deltas[i], lengths_dict[order[i]]) for i, _ in enumerate(order)])
    
    deltas = np.array(deltas)
    bin_values = np.array(bin_values)

    bins = np.linspace(np.min(bin_values), np.max(bin_values), n_bins)
    # print(bins)
    digitized = np.digitize(bin_values, bins)

    bin_means = []
    for i in range(1, len(bins)):
        value = deltas[digitized == i].mean()
        if np.isnan(value):
            value = 0
        bin_means.append((value, bins[i-1], bins[i]))

    return bin_means



def plot_binned(binned1, binned2, title1, title2):

    fig, ax = plt.subplots(1, 2)

    x_axis = [x for x in range(len(binned1))]

    h1, lower, upper = zip(*binned1)
    h2, _, _ = zip(*binned2)
    labels = [f"{int(round(l))}-{int(round(u))}" for l, u in zip(lower, upper)]
    print(labels)

    ax[0].bar(x_axis, h1)
    ax[0].axhline(y=0, color='black', linestyle='--')
    ax[0].set_xticks([x for x in range(len(labels))])
    ax[0].set_xticklabels(labels, rotation=90)
    ax[0].set_title(title1)

    ax[1].bar(x_axis, h2)
    ax[1].axhline(y=0, color='black', linestyle='--')
    ax[1].set_xticks([x for x in range(len(labels))])
    ax[1].set_xticklabels(labels, rotation=90)
    ax[1].set_title(title2)

if __name__ == "__main__":
    plt.rc('legend', fontsize=30)    # legend fontsize
    plt.rc('xtick', labelsize=15)    # fontsize of the tick labels
    plt.rc('ytick', labelsize=15)    # fontsize of the tick labels
    plt.rc('axes', titlesize=20)     # fontsize of the axes title
    plt.rc('axes', labelsize=20)    # fontsize of the x and y labels

    parser = argparse.ArgumentParser()
    parser.add_argument("--file1", type=str, default="output/DukeNet_WoW_warmup_1/result.json")
    parser.add_argument("--file2", type=str, default="output/DukeNet_WoW_Cat_1/result_2.json")
    parser.add_argument("--name1", type=str, default="method 1")
    parser.add_argument("--name2", type=str, default="method 2")
    parser.add_argument("--query_file", type=str, required=True)
    parser.add_argument("--n_bins", type=int, default=5)

    args = parser.parse_args()
    main(args)

from setuptools import setup

setup(
    name='DukeNet',
    version='1.0.0',    
    packages=['DukeNet', 'DukeNetTurnOneHot', 'DukeNetCat', 'dataset', 'evaluation'],
    install_requires=[
        'numpy',
        'scipy',
        'pandas',
        'torch'
    ],
)
